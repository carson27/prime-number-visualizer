import numpy as np
from PIL import Image
import time
import sys

#this program generates an image nxm where every black pixel represents a composite number and every white dot represents a prime number

all_primes = []

def sieve(rows, cols, name):
    n = rows * cols
    prime = np.ones(shape=n, dtype=bool)
    prime[0] = False

    p = 2
    while p * p <= n:
        if prime[p-1]:
            #print(p)
            for i in range(p*2, n+1, p):
                prime[i-1] = False
        p += 1

    draw(prime, rows, cols, name)


def draw(prime, rows, cols, filename):
    arr = np.zeros(shape=(rows, cols, 3), dtype=np.uint8)
    #arr = np.zeros(shape=(rows, cols, 3), dtype=np.uint8)
    index = 0

    counter = 0

    for i in range(rows):
        for j in range(cols):
            if prime[index]:
                arr[i][j] = 255
                all_primes.append(index+1)
                counter += 1
            else:
                arr[i][j] = 0
            index += 1


    print('number of primes:', len(all_primes))
    print('done processing array')
    img = Image.fromarray(arr)
    img.save(filename)



def main():
    rows = int(sys.argv[1])
    cols = int(sys.argv[2])
    filename = sys.argv[3]
    sieve(rows, cols, filename)
    #sieve(32000, 31250, 'billion.png')
    #sieve(1000, 1000, '1000x1000.png')
    #sieve(10000, 10000, '10000x10000.png')
    #sieve(50, 50, '50x50.png')
    #sieve(10000, '10_thousand')


if __name__ == '__main__':
    start_time = time.time()
    main()
    end_time = time.time()
    print('Time to run:', end_time - start_time)